call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'morhetz/gruvbox' 
Plug 'ycm-core/YouCompleteMe'
Plug 'jiangmiao/auto-pairs'
call plug#end()
syntax on
set background=dark
set number
set expandtab
set tabstop=2
map <C-n> :NERDTreeToggle<CR>

